const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const myUser = new Schema({
    username: {
        type: String,
        required: true,
        min: 5,
        max: 20
    },
    password: {
        type: String,
        required: true
    },
    email: {
        type: String,
        validate: {
            validator: (v) => {
                /[a-zA-Z0-9._%-][a-zA-Z-_%]+@[a-zA-Z0-9._%-]+[a-zA-Z]+\.[a-zA-Z]{2,4}/.test(v)
            },

            message: "This is not a valid email"
        }
    }
}, {
    collection: "Member",
    usePushEach: true
});

exports.User = mongoose.model("User", myUser);
