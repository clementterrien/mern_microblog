const express = require('express');
const router = express.Router();
const mongoose = require("mongoose");
const bcrypt = require("bcrypt");
const User = require("../model/memberSchema");

mongoose.connect("mongodb://localhost:27042/test", { useNewUrlParser: true });

/* GET users listing. */
router.get('/', function (req, res, next) {
  User.find({}, (err, user) => {
    res.send(user);
  });
});

router.post('/register', function (req, res, next) {

  let username = req.body.username;
  let password = req.body.password;
  let password_confirm = req.body.password_confirm;
  let email = req.body.email;

  if (password !== password_confirm) {
    res.redirect("/register");
  } else {
    const saltRounds = 10;
    let hash = bcrypt.hashSync(password, saltRounds);
    let user = new User({
      username: username,
      password: hash,
      email: req.body.email,
    });

    user.save().then(user => {
      console.log("Member registerd succefully");
    }).catch(err => {
      console.log(err);
      res.redirect(201, "/")
    });
    res.redirect(201, "/", { message: "Inscription effectué avec succes", loggedIn: true });
  }

});

module.exports = router;
